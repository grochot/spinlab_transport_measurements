import numpy as np
from hardware.agilent34410A import Agilent34410A
from hardware.keithley2400 import Keithley2400

class Average_Results:
    def __init__(self):
        self.results = []

    def GetResult(self, items, device,procedure):
        if device == 1: #Keithley 2400
            if procedure == 1: #Current measurement 2 point
                for i in range(items):
                    self.results.append(float(Keithley2400("GPIB::24").current))
                self.average = np.average(self.results)

            elif procedure == 0: #Voltage measurement 2 point
                for i in range(items):
                    self.results.append(float(Keithley2400("GPIB::24").voltage))
                self.average = np.average(self.results)

            elif procedure == 2: #Resistance measurement 4 point
                for i in range(items):
                    self.results.append(float(Keithley2400("GPIB::24").resistance))
                self.average = np.average(self.results)
            return float(self.average)


        elif device == 2: #Agilent A34410
            for i in range(items):
                self.results.append(float(Agilent34410A("GPIB::3").voltage_dc))
            self.average = np.average(self.results)
            return float(self.average)


