import time
class Onesteptime:

    def starttimemeasure(self):
        self.starttime = time.time
        return self.starttime

    def stoptimemeasure(self):
        self.stoptime = time.time
        return self.stoptime

    def onesteptime(self,steps):
        self.result_step = (self.stoptime - self.starttime)/steps
        return self.result_step

