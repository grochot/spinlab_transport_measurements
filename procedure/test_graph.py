import sys
from PyQt5.QtWidgets import QMainWindow, QApplication, QPushButton, QVBoxLayout, QWidget
from PyQt5.QtCore import *
import pyqtgraph as pg
import time
import numpy as np


class Screen(QMainWindow):
    def __init__(self):
        super(Screen, self).__init__()
        self.initUI()

    def initUI(self):
        self.x = np.array([1,2,3,4])
        self.y = np.array([1,4,9,16])
        self.plt = pg.PlotWidget()
        self.plt.plot(self.x, self.y)

        addBtn = QPushButton('Add Datapoint')
        addBtn.clicked.connect(self.addDataToPlot)
        addBtn.show()

        mainLayout = QVBoxLayout()
        mainLayout.addWidget(addBtn)
        mainLayout.addWidget(self.plt)

        self.mainFrame = QWidget()
        self.mainFrame.setLayout(mainLayout)
        self.setCentralWidget(self.mainFrame)

    def addDataToPlot(self):
        data = {
            'x': 5,
            'y': 25
        }
        np.append(self.x, data['x'])
        np.append(self.y, data['y'])
        self.plt.setData(self.x, self.y)


app = QApplication(sys.argv)
window = Screen()
window.show()
sys.exit(app.exec_())