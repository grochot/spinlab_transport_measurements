from hardware import ametek5209,lakeshore475, zurich_class
import time
import numpy as np
import matplotlib.pyplot as plt
import vector_reader
from hardware import stepper
import calibration
import savetofile

VOLTAGE_FIELD_CONSTANT = calibration.Calibration().measure() # Oersted/Volt
zurich = zurich_class.Zurich()
field = ametek5209.Ametek5209("GPIB::13")
lakeshore = lakeshore475.Lakeshore475("GPIB::12")
vector = vector_reader.VectorReader()
stepp = stepper.Stepper("ASRL3")
save = savetofile.SaveToFile()
####SETTINGS#######
calaz = True
calpol = False



if calaz == True:
    stepp.CallibrationAzim(5000)
    stepp.AskMove(0)
    stepp.GoToZeroAzim()
    stepp.AskMove(0)

if calpol == True:
    stepp.CalibrationPolar(-5000)
    stepp.AskMove(1)
    stepp.GoToZeroPol()
    stepp.AskMove(1)





zurich.initalsett()
zurich.daq.sync()

zurich.setadc(0,0)
zurich.setadc(0,1)
zurich.outputdiff(0,0)
zurich.sigindiff(0)
zurich.siginfloat(0,1)
zurich.siginac(0,1)

zurich.setoscfreq(284,0) #oscillator frequency
zurich.outputrange(0,10)
zurich.outputamplitude(0,1.414) #oscillator amplitude
zurich.siginautorange(0,1)
zurich.setorder(5,0) # filter order
zurich.setorder(5,1)
zurich.settimeconst(0.3,0)
zurich.settimeconst(0.3,1)
zurich.setosc(0,0)
zurich.setosc(0,1)
zurich.sinc(0,1)
zurich.sinc(1,1)
zurich.setharmonic(1,0)
zurich.setharmonic(2,1)
zurich.enabledemod(1,0)
zurich.enabledemod(1,1)

zurich.enableoutput(1,0) #demod, enable
zurich.enableoutput(0,1)
zurich.outputon(0,1)

averaging_rate = 20
vec = "-200,60,110"
flip = True
H_CON = 2000 # constant field


stimulus_H = vector.PrepareVector(vec,flip)
voltage = []
voltage2 =[]
x = []
fig = plt.figure()
ax = fig.add_subplot(211)
ax2 = fig.add_subplot(212)
ax.set_xlabel('Angle [deg]')
ax.set_ylabel('Voltage [V]')
ax2.set_xlabel('Angle [deg]')
ax2.set_ylabel('Voltage [V]')

def fieldtozero():
    for k in range(9, -1, -1):
        field.set_field(((((lakeshore.Field() / VOLTAGE_FIELD_CONSTANT)) / 10)) * (k))
        time.sleep(1)
    print(lakeshore.Field())

def fieldtomax():
    for k in range(0, 10, 1):
        field.set_field(((((H_CON/ VOLTAGE_FIELD_CONSTANT)) / 10)) * (k))
        time.sleep(1)

fieldtomax()
time.sleep(3)
save.Path("4654_harmonic_test", "D:\\Krzysiek\\python_measurement\\spinlab_transport_measurements\\_results")
save.Open()
save.Save("Angle [deg.]" + "\t" + "1f [V]" + "\t" + "2f [V]" + "\n")
save.Close()
for step in stimulus_H:
    field.set_field(H_CON/VOLTAGE_FIELD_CONSTANT)
    time.sleep(0.3)
    stepp.PositionAzim(step)
    stepp.AskMove(0)
    avg = 0
    avg2 = 0
    x.append(step)
    time.sleep(1)
    for samp in range(averaging_rate):
        sample = zurich.getsample(0)
        sample2 = zurich.getsample(1)
        avg += np.abs(sample['x'][0] + 1j * sample['y'][0])
        avg2 += np.abs(sample2['x'][0] + 1j * sample2['y'][0])
    voltage.append(avg/averaging_rate)
    voltage2.append(avg2/averaging_rate)
    ax.plot(x, voltage, "-bo")
    ax2.plot(x, voltage2, "-go")
    save.Open()
    save.Save(str(step) + "\t" + str(avg / averaging_rate) + "\t" + str(avg2 / averaging_rate) + "\n")
    save.Close()
    plt.pause(0.001)


fieldtozero()
stepp.PositionAzim(0)
zurich.outputon(0,0)
plt.show()




