#import csv
from time import gmtime, strftime
#from gui.parameters import Parameters
#from gui.gui_test import Window
import os
from procedure.buffor import Buffor

class SaveToFile(Buffor):
    def Path(self, name, path):
        #self.path = Window().directory
        self.date = strftime("%d_%m_%Y_%H_%M_%S", gmtime())
        #self.name = Parameters().readfromfile()
        #self.file = self.path+"/"+self.name["name"] + "_" + self.date
        #self.file = self.name["name"] + "_" + self.date
        self.file = path + "\\" + name + "_" + self.date+".txt"
        return self.file
    def Open(self):
        self.savedfile = open(self.file, 'a')
        #self.savedfile = csv.writer(self.savedfile, delimiter='', quoting=csv.QUOTE_NONE)
    def Save(self, record):
        self.savedfile.write(record)
    def Close(self):
        self.savedfile.close()
    def SaveToExcel(self, path,name):
        self.date = strftime("%d_%m_%Y_%H_%M_%S", gmtime())
        self.file = path + "\\" + name + "_" + self.date + ".xlsx"
        self.getter().to_excel(self.file, index = False)


# print(os.getcwd())
# obiekt = SaveToFile()
# obiekt.initialize()
# obiekt.add([1,2,3,4])
# obiekt.add([3,4,5,6,6,7])
# obiekt.SaveToExcel("D:\Krzysiek\python_measurement\spinlab_transport_measurements\procedure", "test")


