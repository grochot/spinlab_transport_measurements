from hardware.agilent34410A import Agilent34410A
from hardware.keithley2400 import Keithley2400


class ContMeasure:

    def continouskeithleyset(self):
        self.keithley = Keithley2400("GPIB::24")
        self.keithley.reset()
        self.keithley.use_front_terminals()
        self.keithley.apply_voltage()
        self.keithley.source_voltage_range = 2
        self.keithley.compliance_current = 1e-3
        self.keithley.source_voltage = 0.01
        self.keithley.enable_source()

    def continouskeithleyget(self):
        self.result_current = self.keithley.measure_current()
        self.result_voltage = self.keithley.mean_voltage()
        return self.result_voltage/self.result_current

    def continousagilentset(self):
        self.agilent = Agilent34410A("GPIB:3")

    def continousagilentget(self):
        self.result = self.agilent.resistance
        return self.result







