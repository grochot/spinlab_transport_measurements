from hardware.keithley2400 import Keithley2400
from hardware.agilent34410A import Agilent34410A
from hardware.lakeshore475 import Lakeshore475
from hardware.ametek5209 import Ametek5209
from procedure.calibration import Calibration
from procedure.vector_reader import VectorReader
from procedure.average import Average_Results
import time
import os
from procedure.savetofile import SaveToFile

class TMR_Agilent(SaveToFile):
    def __init__(self):
        self.rV = []
        self.rI = []
        self.rH = []
        self.rR = []
        self.initial()
        self.setfield()
        for step in self.stimulus_H:
            self.measure(step)
        self.fieldtozero()

    def initial(self):
        self.agilent = Agilent34410A("GPIB::3")
        self.gaussmeter = Lakeshore475("GPIB::12")
        self.fieldcontrol = Ametek5209("GPIB::13")
        self.TIME_DELAY=0.3 #Time Delay
        self.VOLTAGE_FIELD_CONST = Calibration().measure()  #Calibration procedure
        self.initialize()
        self.vector = "-100,20,100" #Set Vector
        self.stimulus_H = VectorReader().PrepareVector(self.vector,False)
        print(self.stimulus_H)

        #Set devices
        # self.keithley = Keithley2400("GPIB::24")
        # self.keithley.reset()
        # self.keithley.use_front_terminals()
        # self.keithley.apply_voltage()
        # self.keithley.source_voltage_range = 2
        # self.keithley.compliance_current = 1e-3
        # self.keithley.source_voltage = 0.01
        # self.keithley.enable_source()
        # self.keithley.disable_source()
        # keithley.reset()
        # keithley.use_front_terminals()
        # keithley.apply_current()
        # keithley.source_current_range = 10e-3
        # keithley.compliance_voltage = 2
        # keithley.source_current = 1e-4
        # keithley.disable_source()


##############SET FIELD#########################################
    def setfield(self):
        for i in range(5):
            self.fieldcontrol.set_field(((((self.stimulus_H[0]/self.VOLTAGE_FIELD_CONST))/5))*(i+1))
            time.sleep(1)

        time.sleep(0.7)

################MEASURE########################################
    def measure(self, step):
        self.fieldcontrol.set_field((step/self.VOLTAGE_FIELD_CONST))
        time.sleep(self.TIME_DELAY)
        self.rH.append(self.gaussmeter.Field())
        #self.keithley.enable_source()
        self.rR.append(Average_Results().GetResult(3, self.agilent))




    def fieldtozero(self):
        #self.keithley.disable_source()
        for k in range(4,-1 ,-1):
            self.fieldcontrol.set_field(((((100/self.VOLTAGE_FIELD_CONST))/5))*(k))
            time.sleep(1)


# test = ResistanceField()
# test.initial()
# test.setfield()
#
# for step in test.stimulus_H:
#     test.measure(step)
#
# test.fieldtozero()
# test.add(test.rH)
# test.add(test.rR)
# test.SaveToExcel(str(os.getcwd()),"test")



