from hardware.ametek5209 import Ametek5209
from hardware.lakeshore475 import Lakeshore475
import time
import numpy as np
from gui.settings import Settings
# import matplotlib.pyplot as plt
from scipy import stats

class Calibration:
    VOLTAGE_START = 0.5
    VOLTAGE_STOP = 2
    STEP = 0.1
    stimulus_V = np.arange(VOLTAGE_START, VOLTAGE_STOP, STEP)
    ret_H = np.flip(np.arange(0, VOLTAGE_STOP, STEP))
    value_H = []

    def wyswietlenie(self):
        print(self.stimulus_V)
        print(self.ret_H)

    def linearfit(self):
        slope, intercept, r_value, p_value, std_err = stats.linregress(self.stimulus_V, self.value_H)
        coeff = slope + intercept
        return coeff
    def measure(self):
        set_voltage = Ametek5209("GPIB::13")
        measured_H = Lakeshore475("GPIB::12")
        for step_V in self.stimulus_V:
            set_voltage.set_field(step_V)
            time.sleep(0.5)
            self.value_H.append(measured_H.Field())
            time.sleep(0.1)
        for i in self.ret_H:
            set_voltage.set_field(i)
            time.sleep(0.1)
        slope2 = self.linearfit()
        Settings().settings["field"]["calconst"] = slope2
        return float(slope2)

    # def wykres(self):
    #     plt.plot(self.stimulus_V, self.value_H, 'ro')
    #     plt.show()






