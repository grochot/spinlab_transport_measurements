from hardware.keithley2400 import Keithley2400
from hardware.agilent34410A import Agilent34410A
from hardware.lakeshore475 import Lakeshore475
from hardware.ametek5209 import Ametek5209
from procedure.vector_reader import VectorReader
from procedure.average import Average_Results
from settings import Settings
from parameters import Parameters
import time
from procedure.savetofile import SaveToFile

class TMR_Keithley(SaveToFile):
    def __init__(self):
        self.state = True


    def initial(self):
        self.agilent = Agilent34410A("GPIB::3")
        self.gaussmeter = Lakeshore475("GPIB::12")
        self.fieldcontrol = Ametek5209("GPIB::13")
        self.TIME_DELAY= Settings().settings["field"]["field_delay"] #Time Delay
        self.VOLTAGE_FIELD_CONST = Settings().settings["field"]["calconst"] #Field_const
        self.initialize()
        self.vector = Parameters().parameters["stimulus"] #Set Vector
        self.stimulus_H = VectorReader().PrepareVector(self.vector,False)
        self.procedure = 0


       # Set devices
        self.keithley = Keithley2400("GPIB::24")
        self.keithley.reset()
        self.keithley.use_front_terminals()
        if self.procedure == 1:
            self.keithley.apply_voltage()
            self.keithley.source_voltage_range = 2
            self.keithley.compliance_current = 1e-3
            self.keithley.source_voltage = 0.01
            self.keithley.measure_current()
            self.keithley.disable_source()

        elif self.procedure == 0:
            self.keithley.apply_current()
            self.keithley.source_current_range = 0.02
            self.keithley.compliance_voltage = 4
            self.keithley.source_current = 0.001
            self.keithley.measure_voltage()
            self.keithley.disable_source()






        # keithley.reset()
        # keithley.use_front_terminals()
        # keithley.apply_current()
        # keithley.source_current_range = 10e-3
        # keithley.compliance_voltage = 2
        # keithley.source_current = 1e-4
        # keithley.disable_source()


##############SET FIELD#########################################
    def setfield(self):
        self.stepsslowfield = (self.stimulus_H[0]/self.VOLTAGE_FIELD_CONST)/Settings().settings["field"]["fieldslowstep"]
        for i in range(int(self.stepsslowfield)):
            self.fieldcontrol.set_field(((((self.stimulus_H[0]/self.VOLTAGE_FIELD_CONST))/int(self.stepsslowfield)))*(i+1))
            time.sleep(1)

        time.sleep(0.7)

################MEASURE########################################
    def measure(self, step):
        self.fieldcontrol.set_field((step/self.VOLTAGE_FIELD_CONST))
        time.sleep(self.TIME_DELAY)
        self.rH=(self.gaussmeter.Field())
        self.keithley.enable_source()
        self.rI=(Average_Results().GetResult(3, 1, 1))
        self.rV=(Average_Results().GetResult(3, 1, 0))

       # return self.rH, self.rI, self.rV




    def fieldtozero(self):
        self.keithley.disable_source()
        if Parameters().parameters["fieldtozero"] == True:
            self.last_point = len(self.stimulus_H)
            for k in range(self.stepsslowfield,-1 ,-1):
                self.fieldcontrol.set_field(((((self.stimulus_H[self.last_point]/self.VOLTAGE_FIELD_CONST))/self.stepsslowfield))*(k))
                time.sleep(1)
            self.state = False



    # def generate_random_test(self):
    #     for i in range(10):
    #         self.rH.append(random.random())
    #         self.rV.append(random.random())
    #         time.sleep(1)
    #     self.state = False



# test = TMR_Keithley()
# test.initial()
# test.setfield()
#
# for step in test.stimulus_H:
#     test.measure(step)
#
# test.fieldtozero()
# test.rR = test.rV/test.rI
# test.add(test.rH)
# test.add(test.rI)
# test.add(test.rV)
# test.add(test.rR)
# test.SaveToExcel(str(os.getcwd()),"test")

# test.keithley.enable_source()
# test.keithley.measure_current()  # Sets up to measure voltage
# print(test.keithley.current)
# print(test.keithley.current) # Prints the voltage in Volts
# test.keithley.disable_source()