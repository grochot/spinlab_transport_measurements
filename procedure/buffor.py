import pandas as pd

class Buffor:
    counter = 0
    def initialize(self):
        self.buffor = pd.DataFrame(index = range(10000))

    def add(self, list):
        self.counter = self.counter + 1
        self.buffor[self.counter] = pd.Series(list)

    def getter(self):
        return self.buffor