import numpy as np


class VectorReader:

    def PrepareVector(self, vector_in, flip):
        vector_table = vector_in.split(",")
        val=[]
        step=[]
        value_final_final = []

        for i in range(len(vector_table)):
            if i%2==0:
                val.append(float(vector_table[i]))
            else:
                step.append(int(vector_table[i]))

        for k in range(len(step)):
            value_final = np.linspace(val[k], val[k+1], step[k], endpoint=False)
            value_final_final = np.concatenate((value_final_final, value_final))

        value_final_final_final = value_final_final

        if flip == True:
            value_final_ret = np.flip(value_final_final)
            value_final_final_final = np.concatenate((value_final_final,value_final_ret))


        return value_final_final_final


# obiekt = VectorReader()
# print(obiekt.PrepareVector("-5000,10,-1000,60,1000,10,5000", True))




