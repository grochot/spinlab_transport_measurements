from hardware import ametek5209,lakeshore475, zurich_class
#from gui.gui_test import Window
import time
import numpy as np
import matplotlib.pyplot as plt
#import vector_reader
import calibration
#import savetofile

def zurichLockin_measurement(gui):
    VOLTAGE_FIELD_CONSTANT = calibration.Calibration().measure()  # Oersted/Volt
    print(VOLTAGE_FIELD_CONSTANT)
    field = ametek5209.Ametek5209("GPIB::13")
    lakeshore = lakeshore475.Lakeshore475("GPIB::12")
    save = savetofile.SaveToFile()
    vector = vector_reader.VectorReader()

    zurich = zurich_class.Zurich()

    def settings_GUIread():
        #Signal Input
        zurich.siginautorange(signal=1, auto=1)
        zurich.siginrange(signal, range=gui.voltageInputRange.value())
        zurich.siginscaling(signal, scaling=1)
        zurich.siginac(signal, ac=0)
        zurich.siginimp50(signal, imp50=0)
        zurich.sigindiff(diff=0)
        zurich.siginfloat(signal, float=0)
        #Signal Output


        zurich.setadc(0,1);

    def measurementParams_GUIread():
        a = []

    zurich.initalsett()
    settings_GUIread()
    measurementParams_GUIread()

###### Stara procedura Krzyska (bez gui, testowa)

#VOLTAGE_FIELD_CONSTANT = calibration.Calibration().measure() # Oersted/Volt
#print(VOLTAGE_FIELD_CONSTANT)
#zurich = zurich_class.Zurich()
#field = ametek5209.Ametek5209("GPIB::13")
#lakeshore = lakeshore475.Lakeshore475("GPIB::12")
#save = savetofile.SaveToFile()
#vector = vector_reader.VectorReader()

#zurich.initalsett()
zurich.daq.sync()

zurich.setadc(0,0)
zurich.setadc(0,1)
zurich.outputdiff(0,0)
zurich.sigindiff(0)
zurich.siginfloat(0,1)
zurich.siginac(0,1)

zurich.setoscfreq(284,0)
zurich.outputrange(0,10)
zurich.outputamplitude(0,1.414)
zurich.siginautorange(0,1)
zurich.setorder(4,0)
zurich.setorder(4,1)
zurich.settimeconst(0.3,0)
zurich.settimeconst(0.3,1)
zurich.sinc(0,1)
zurich.sinc(1,1)

zurich.setosc(0,0)
zurich.setosc(0,1)
zurich.setharmonic(1,0)
zurich.setharmonic(2,1)
zurich.enabledemod(1,0)
zurich.enabledemod(1,1)

zurich.enableoutput(1,0) #demod, enable
zurich.enableoutput(0,1)
zurich.outputon(0,1)

averaging_rate = 20
vec = "-9000,10,-1500,60,1500,10,9000"
flip = True


stimulus_H = vector.PrepareVector(vec, flip)
voltage = []
voltage2 =[]
x = []
fig = plt.figure()
ax = fig.add_subplot(211)
ax2 = fig.add_subplot(212)
ax.set_xlabel('Field [Oe]')
ax.set_ylabel('Amplitude R [V]')
ax2.set_xlabel('Field [Oe]')
ax2.set_ylabel('Amplitude R [V]')

def fieldtozero():
    for k in range(9, -1, -1):
        field.set_field(((((lakeshore.Field() / VOLTAGE_FIELD_CONSTANT)) / 10)) * (k))
        time.sleep(1)
    print(lakeshore.Field())

def fieldtomax():
    for k in range(0, 10, 1):
        field.set_field(((((vector.PrepareVector(vec,flip)[0] / VOLTAGE_FIELD_CONSTANT)) / 10)) * (k))
        time.sleep(1)

fieldtomax()
time.sleep(3)
save.Path("4654_harmonic_test", "D:\\Krzysiek\\python_measurement\\spinlab_transport_measurements\\_results")
save.Open()
save.Save("Field [Oe]" + "\t" + "1f [V]" + "\t" + "2f [V]" + "\n")

for step in stimulus_H:
    field.set_field(step/VOLTAGE_FIELD_CONSTANT)
    time.sleep(0.5)
    avg = 0
    avg2 = 0
    h = lakeshore.Field()
    x.append(h)
    for samp in range(averaging_rate):
        sample = zurich.getsample(0)
        sample2 = zurich.getsample(1)
        avg += np.abs(sample['x'][0] + 1j * sample['y'][0])
        avg2 += np.abs(sample2['x'][0] + 1j * sample2['y'][0])
    voltage.append(avg/averaging_rate)
    voltage2.append(avg2/averaging_rate)
    ax.plot(x, voltage, "-bo")
    ax2.plot(x, voltage2, "-go")
    save.Save(str(h)+"\t"+str(avg/averaging_rate)+"\t"+str(avg2/averaging_rate)+"\n")
    plt.pause(0.001)

save.Close()
fieldtozero()
zurich.outputon(0,0)
plt.show()




