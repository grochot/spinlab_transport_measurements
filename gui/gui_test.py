import sys
sys.path.append("/Users/grochot/python/spinlab_transport_measurements/")
from PyQt5 import QtCore, QtWidgets, uic
from PyQt5.QtWidgets import QFileDialog
import sys
import random
import time
from procedure.buffor import Buffor
from procedure.savetofile import SaveToFile
from matplotlib.backends.backend_qt5agg import (NavigationToolbar2QT as NavigationToolbar)

from parameters import Parameters
from settings import Settings
from procedure.calibration import Calibration
from procedure import harmonic, tmr_keathley_2400, tmr_agilent,tmr_4

qtCreatorFile = "gui_os.ui"
Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)

class Example(QtCore.QObject):

    refreshPlot = QtCore.pyqtSignal()
    refreshBar = QtCore.pyqtSignal()

    def __init__(self, parent=None):
     super(self.__class__, self).__init__(parent)

     #Buffor initialization
     self.buffor = Buffor()
     self.buffor.initialize()
     # Create a gui object.
     self.gui = Window()
     self.gui.ProgressBar.setValue(0)

     # Setup the worker object and the worker_thread.
     self.worker = WorkerObject()
     self.worker_thread = QtCore.QThread()
     self.worker.moveToThread(self.worker_thread)
     self.worker_thread.start()

     # Make any cross object connections.
     self._connectSignals()

     self.gui.show()
     self.gui.par_stimulusunit.setText("Oe")
     self.gui.par_sourceunit.setText("A")

    #Connect signals
    def _connectSignals(self):
     self.gui.StartButton.clicked.connect(self.gui.startMeasurement)
     self.gui.StopButton.clicked.connect(self.forceWorkerReset)
     self.gui.par_accept.clicked.connect(self.gui.ReadParameters)
     self.gui.par_lastvalues.clicked.connect(self.gui.FillFromFile)
     self.gui.par_reset.clicked.connect(self.gui.ResetParameters)
     self.gui.path_button.clicked.connect(self.gui.pathbutton)
     self.gui.sett_LoadButton.clicked.connect(self.gui.FillFromFileSettings)
     self.gui.sett_SaveButton.clicked.connect(self.gui.SaveSettings)
     self.gui.par_procedure.currentIndexChanged.connect(self.gui.tmrselect)
     self.gui.par_accept.clicked.connect(self.gui.AddToTable)
     self.gui.source_selector.currentIndexChanged.connect(self.gui.sourceselector)
     self.gui.multimeter_selector.currentIndexChanged.connect(self.gui.multimeterselector)
     self.gui.source_source.currentIndexChanged.connect(self.gui.source_unit_behaviour)
     self.gui.gaussmeter_selector.currentIndexChanged.connect(self.gui.gaussmeterselector)
     self.gui.stepper_selector.currentIndexChanged.connect(self.gui.stepperselector)
     self.gui.par_steppermode.currentIndexChanged.connect(self.gui.steppertypebehaviour)
     self.gui.field_selector.currentIndexChanged.connect(self.gui.fieldselector)
     self.gui.lockin_selector.currentIndexChanged.connect(self.gui.lockinselector)
     self.gui.lockin_slider1.valueChanged.connect(self.gui.zurichLockinChangePage)
     self.gui.field_calibrate.clicked.connect(self.worker.calib)
     self.worker.refreshPlot.connect(self.gui.update)
     self.worker.refreshBar.connect(self.gui.ProgressBarValue)
     self.parent().aboutToQuit.connect(self.forceWorkerQuit)

    #Force reset Worker
    def forceWorkerReset(self):
     if self.worker_thread.isRunning():
      self.worker_thread.terminate()
      self.worker_thread.wait()
      self.worker_thread.start()

    def forceWorkerQuit(self):
     if self.worker_thread.isRunning():
      self.worker_thread.terminate()
      self.worker_thread.wait()

#################################Second thread####################################################################
class WorkerObject(QtCore.QObject, tmr_keathley_2400.TMR_Keithley, SaveToFile): #Tutaj musi byc realizowany pomiar
    refreshPlot = QtCore.pyqtSignal()
    refreshBar = QtCore.pyqtSignal()
    testSignal = QtCore.pyqtSignal()
    results_x = []
    results_y = []

    def __init__(self, parent=None):
     super(self.__class__, self).__init__(parent)

    @QtCore.pyqtSlot()
    def GetPoint(self):
        self.initial()

        self.setfield()

        for step in self.stimulus_H:
            self.measure(step)
            time.sleep(2)
            self.results_x.append(self.rH)
            self.results_y.append(self.rV)
            self.RefreshPlot.emit()
            print(self.results_y)

        self.fieldtozero()
        Example().buffor.add(self.results_x)
        Example().buffor.add(self.results_y)
        self.savebackup()


    def calib(self):
        self.cal_val = Calibration().measure()
        self.field_cal_const.setText(self.cal_val)

    def savebackup(self):
        self.SaveToExcel("D:\Krzysiek\python_measurement\spinlab_transport_measurements\procedure", "test")




###############################GUI######################################
class Window(QtWidgets.QMainWindow, Ui_MainWindow, WorkerObject):

    def __init__(self):
     QtWidgets.QMainWindow.__init__(self)
     Ui_MainWindow.__init__(self)
     self.completed = 0
     self.setupUi(self)
     self.FillParametersInit()
     self.FillSettingsInit()
     self.initstack()
     self.initcombo()
     #self.addToolBar(NavigationToolbar(self.MplWidget.canvas, self)) ?? Pytanie czy potrzebne


    @QtCore.pyqtSlot()
    def startMeasurement(self):
        if (self.par_procedure.currentIndex() == 2):
            pass  # harmonic procedure

        elif (self.par_procedure.currentIndex() == 0):
            self.worker.GetPoint()

        elif (self.par_procedure.currentIndex() == 1):
            pass  # TMR4 procedure class

    @QtCore.pyqtSlot()
    def update(self):
        self.MplWidget.canvas.axes.clear()
        self.MplWidget.canvas.axes.plot(self.results_x, self.results_y, "-o")
        self.MplWidget.canvas.draw()
        print(self.results_y)

############################################### Table ###############################################
    @QtCore.pyqtSlot()
    def AddToTable(self):
        self.task = Parameters().readfromfile()
        self.rowPosition = self.MeasurementsList.rowCount()
        self.MeasurementsList.insertRow(self.rowPosition)
        self.MeasurementsList.setItem(0, self.rowPosition, QtWidgets.QTableWidgetItem(self.task["name"]+"_"+self.task["par_procedure"]+"_"+str(self.task["source"])))
        self.AddToChannel()

    @QtCore.pyqtSlot()
    def AddToChannel(self):
        self.ChannelsList.insertRow(0)
        self.ChannelsList.insertRow(1)
        self.ChannelsList.insertRow(2)
        self.ChannelsList.insertRow(3)
        self.ChannelsList.insertRow(4)
        self.ChannelsList.insertRow(5)
        self.ChannelsList.setItem(0, 0, QtWidgets.QTableWidgetItem("Resistance"))
        self.ChannelsList.setItem(0, 1, QtWidgets.QTableWidgetItem("Field"))
        self.ChannelsList.setItem(0, 2, QtWidgets.QTableWidgetItem("Voltage"))
        self.ChannelsList.setItem(0, 3, QtWidgets.QTableWidgetItem("Current"))
        self.ChannelsList.setItem(0, 4, QtWidgets.QTableWidgetItem("Angle"))
        self.ChannelsList.setItem(0, 5, QtWidgets.QTableWidgetItem("AC Voltage"))



    @QtCore.pyqtSlot()
    def zurichLockinChangePage(self):
        if(self.lockin_slider1.value() > 50):
            self.lockin_stack.setCurrentIndex(0)
        else:
            self.lockin_stack.setCurrentIndex(1)


    @QtCore.pyqtSlot()
    def ProgressBarValue(self):

        while self.completed < 100:
            self.completed += 1
            self.ProgressBar.setValue(self.completed)
            break



###################################################PARAMETERS##########################################################

    @QtCore.pyqtSlot()
    def FillParametersInit(self):
        self.param = Parameters().parameters
        self.par_name.setText(self.param["name"])
        self.par_stimulus.setText(self.param["stimulus"])
        self.par_source.setText(self.param["source"])
        self.par_biasvalue.setText(self.param["biasvalue"])
        self.par_azim.setText(self.param["azim"])
        self.par_polar.setText(self.param["polar"])
        self.par_frequency.setText(self.param["freq"])
        self.par_temp.setText(self.param["temp"])
        self.par_pulsewidth.setText(self.param["pulsewidth"])
        self.par_pulseperiod.setText(self.param["pulseperiod"])
        self.par_fieldbias.setText(self.param["fieldbias"])
        self.par_fieldangle.setText(self.param["fieldangle"])

    @QtCore.pyqtSlot()
    def ReadParameters(self):
        Parameters().addtofile("name", self.par_name.text())
        Parameters().addtofile("stimulus", self.par_stimulus.text())
        Parameters().addtofile("source", self.par_source.text())
        Parameters().addtofile("biasvalue", self.par_biasvalue.text())
        Parameters().addtofile("azim", self.par_azim.text())
        Parameters().addtofile("polar", self.par_polar.text())
        Parameters().addtofile("freq", self.par_frequency.text())
        Parameters().addtofile("temp", self.par_temp.text())
        Parameters().addtofile("pulsewidth", self.par_pulsewidth.text())
        Parameters().addtofile("pulseperiod", self.par_pulseperiod.text())
        Parameters().addtofile("fieldbias", self.par_fieldbias.text())
        Parameters().addtofile("fieldangle", self.par_fieldangle.text())
        Parameters().addtofile("par_procedure", self.par_procedure.currentText())
        Parameters().addtofile("par_steppermode", self.par_steppermode.currentText())
        Parameters().savetofile()
    @QtCore.pyqtSlot()
    def FillFromFile(self):

        self.param = Parameters().readfromfile()
        self.par_name.setText(self.param["name"])
        self.par_stimulus.setText(self.param["stimulus"])
        self.par_source.setText(self.param["source"])
        self.par_biasvalue.setText(self.param["biasvalue"])
        self.par_azim.setText(self.param["azim"])
        self.par_polar.setText(self.param["polar"])
        self.par_frequency.setText(self.param["freq"])
        self.par_temp.setText(self.param["temp"])
        self.par_pulsewidth.setText(self.param["pulsewidth"])
        self.par_pulseperiod.setText(self.param["pulseperiod"])
        self.par_fieldbias.setText(self.param["fieldbias"])
        self.par_fieldangle.setText(self.param["fieldangle"])

    def ResetParameters(self):
        self.par_name.setText("")
        self.par_stimulus.setText("")
        self.par_source.setText("")
        self.par_biasvalue.setText("")
        self.par_azim.setText("")
        self.par_polar.setText("")
        self.par_frequency.setText("")
        self.par_temp.setText("")
        self.par_pulsewidth.setText("")
        self.par_pulseperiod.setText("")
        self.par_fieldbias.setText("")
        self.par_fieldangle.setText("")


################################################ SETTINGS  ##########################################################
    def FillSettingsInit(self):
        self.set_general = Settings().settings["general"]
        self.set_source = Settings().settings["sourcemeter"]
        self.set_multimeter = Settings().settings["multimeter"]
        self.set_gaussmeter = Settings().settings["gaussmeter"]
        self.set_zurichLockin = Settings().settings["zurichLockin"]
        self.set_field = Settings().settings["field"]


        self.user_selector.setCurrentIndex(self.set_general["name"])
        self.path_text_line.setText(self.set_general["pathtosave"])

        self.source_selector.setCurrentIndex(self.set_source["selector"])
        self.source_port.setCurrentIndex(self.set_source["port"])
        self.source_NPLC.setValue(self.set_source["NPLC"])
        self.source_source.setCurrentIndex(self.set_source["source"])
        self.source_mode.setChecked(self.set_source["mode"])
        self.source_limit.setValue(self.set_source["limit"])
        self.source_average.setValue(self.set_source["average"])
        self.source_delay.setValue(self.set_source["delay"])

        self.multimeter_selector.setCurrentIndex(self.set_multimeter["selector"])
        self.multimeter_port.setCurrentIndex(self.set_multimeter["port"])
        self.multimeter_function.setCurrentIndex(self.set_multimeter["function"])
        self.multimeter_autorange.setChecked(self.set_multimeter["autorange"])
        self.multimeter_resolution.setValue(self.set_multimeter["resolution"])
        self.multimeter_range.setValue(self.set_multimeter["range"])
        self.multimeter_average.setValue(self.set_multimeter["average"])

        self.gaussmeter_selector.setCurrentIndex(self.set_gaussmeter["selector"])
        self.gaussmeter_port.setCurrentIndex(self.set_gaussmeter["port"])
        self.gaussmeter_autorange.setChecked(self.set_gaussmeter["autorange"])
        self.gaussmeter_dcresol.setValue(self.set_gaussmeter["DCresol"])
        self.gaussmeter_range.setValue(self.set_gaussmeter["range"])
        self.gaussmeter_average.setValue(self.set_gaussmeter["average"])

        self.lockin_selector.setCurrentIndex(self.set_zurichLockin["selector"])
        self.zurich_voltageInputRange.setValue(self.set_zurichLockin["zurich_voltageInputRange"])
        self.zurich_voltageInputAutoRange.setChecked(self.set_zurichLockin["zurich_voltageInputAutoRange"])
        self.zurich_voltageInputScaling.setValue(self.set_zurichLockin["zurich_voltageInputScaling"])
        self.zurich_voltageInputAC.setChecked(self.set_zurichLockin["zurich_voltageInputAC"])
        self.zurich_voltageInput50Ohm.setChecked(self.set_zurichLockin["zurich_voltageInput50Ohm"])
        self.zurich_voltageInputDiff.setChecked(self.set_zurichLockin["zurich_voltageInputDiff"])
        self.zurich_voltageInputFloat.setChecked(self.set_zurichLockin["zurich_voltageInputFloat"])
        self.zurich_currentInputRange.setValue(self.set_zurichLockin["zurich_currentInputRange"])
        self.zurich_currentInputAutoRange.setChecked(self.set_zurichLockin["zurich_currentInputAutoRange"])
        self.zurich_currentInputScaling.setValue(self.set_zurichLockin["zurich_currentInputScaling"])
        self.zurich_currentInputFloat.setChecked(self.set_zurichLockin["zurich_currentInputFloat"])
        self.zurich_signalOutputON.setChecked(self.set_zurichLockin["zurich_signalOutputON"])
        self.zurich_signalOutput50Ohm.setChecked(self.set_zurichLockin["zurich_signalOutput50Ohm"])
        self.zurich_signalOutputRange.setValue(self.set_zurichLockin["zurich_signalOutputRange"])
        self.zurich_signalOutputAdd.setChecked(self.set_zurichLockin["zurich_signalOutputAdd"])
        self.zurich_signalOutputDiff.setChecked(self.set_zurichLockin["zurich_signalOutputDiff"])
        self.zurich_oscillator1Freq.setValue(self.set_zurichLockin["zurich_oscillator1Freq"])
        self.zurich_oscillator2Freq.setValue(self.set_zurichLockin["zurich_oscillator2Freq"])
        self.zurich_oscillator3Freq.setValue(self.set_zurichLockin["zurich_oscillator3Freq"])
        self.zurich_oscillator4Freq.setValue(self.set_zurichLockin["zurich_oscillator4Freq"])
        self.zurich_demod1Osc.setCurrentIndex(self.set_zurichLockin["zurich_demod1Osc"])
        self.zurich_demod2Osc.setCurrentIndex(self.set_zurichLockin["zurich_demod2Osc"])
        self.zurich_demod3Osc.setCurrentIndex(self.set_zurichLockin["zurich_demod3Osc"])
        self.zurich_demod4Osc.setCurrentIndex(self.set_zurichLockin["zurich_demod4Osc"])
        self.zurich_demod1Harm.setCurrentIndex(self.set_zurichLockin["zurich_demod1Harm"])
        self.zurich_demod2Harm.setCurrentIndex(self.set_zurichLockin["zurich_demod2Harm"])
        self.zurich_demod3Harm.setCurrentIndex(self.set_zurichLockin["zurich_demod3Harm"])
        self.zurich_demod4Harm.setCurrentIndex(self.set_zurichLockin["zurich_demod4Harm"])
        self.zurich_demod1Signal.setCurrentIndex(self.set_zurichLockin["zurich_demod1Signal"])
        self.zurich_demod2Signal.setCurrentIndex(self.set_zurichLockin["zurich_demod2Signal"])
        self.zurich_demod3Signal.setCurrentIndex(self.set_zurichLockin["zurich_demod3Signal"])
        self.zurich_demod4Signal.setCurrentIndex(self.set_zurichLockin["zurich_demod4Signal"])
        self.zurich_demod1Enable.setChecked(self.set_zurichLockin["zurich_demod1Enable"])
        self.zurich_demod2Enable.setChecked(self.set_zurichLockin["zurich_demod2Enable"])
        self.zurich_demod3Enable.setChecked(self.set_zurichLockin["zurich_demod3Enable"])
        self.zurich_demod4Enable.setChecked(self.set_zurichLockin["zurich_demod4Enable"])
        self.zurich_demod1FilterOrder.setCurrentIndex(self.set_zurichLockin["zurich_demod1FilterOrder"])
        self.zurich_demod2FilterOrder.setCurrentIndex(self.set_zurichLockin["zurich_demod2FilterOrder"])
        self.zurich_demod3FilterOrder.setCurrentIndex(self.set_zurichLockin["zurich_demod3FilterOrder"])
        self.zurich_demod4FilterOrder.setCurrentIndex(self.set_zurichLockin["zurich_demod4FilterOrder"])

        self.field_cal_const.setValue(self.set_field["calconst"])
        self.field_delay.setValue(self.set_field["delayafterfield"])
        self.field_fieldtozero.setChecked(True)
        self.field_step.setValue(self.set_field["fieldslowstep"])

    def ReadSettings(self):
        self.get_general = Settings().settings["general"]
        self.get_source = Settings().settings["sourcemeter"]
        self.get_multimeter = Settings().settings["multimeter"]
        self.get_gaussmeter = Settings().settings["gaussmeter"]
        self.get_zurichLockin = Settings().settings["zurichLockin"]
        self.get_field = Settings().settings["field"]

        self.get_general["name"] = self.user_selector.currentIndex()
        self.get_general["pathtosave"] = self.path_text_line.text()

        self.get_source["selector"] = self.source_selector.currentIndex()
        self.get_source["port"] = self.source_port.currentIndex()
        self.get_source["NPLC"] = self.source_NPLC.value()
        self.get_source["source"] = self.source_source.currentIndex()
        self.get_source["mode"] = self.source_mode.isChecked()
        self.get_source["limit"] = self.source_limit.value()
        self.get_source["average"] = self.source_average.value()
        self.get_source["delay"] = self.source_delay.value()

        self.get_multimeter["selector"] = self.multimeter_selector.currentIndex()
        self.get_multimeter["port"] = self.multimeter_port.currentIndex()
        self.get_multimeter["function"] = self.multimeter_function.currentIndex()
        self.get_multimeter["autorange"] = self.multimeter_autorange.isChecked()
        self.get_multimeter["resolution"] = self.multimeter_resolution.value()
        self.get_multimeter["range"] = self.multimeter_range.value()
        self.get_multimeter["average"] = self.multimeter_average.value()

        self.get_gaussmeter["selector"] = self.gaussmeter_selector.currentIndex()
        self.get_gaussmeter["port"] = self.gaussmeter_port.currentIndex()
        self.get_gaussmeter["autorange"] = self.gaussmeter_autorange.isChecked()
        self.get_gaussmeter["DCresol"] = self.gaussmeter_dcresol.value()
        self.get_gaussmeter["range"] = self.gaussmeter_range.value()
        self.get_gaussmeter["average"] = self.gaussmeter_average.value()

        self.get_zurichLockin["selector"] = self.lockin_selector.currentIndex()
        self.get_zurichLockin["zurich_voltageInputRange"] = self.zurich_voltageInputRange.value()
        self.get_zurichLockin["zurich_voltageInputAutoRange"] = self.zurich_voltageInputAutoRange.isChecked()
        self.get_zurichLockin["zurich_voltageInputScaling"] = self.zurich_voltageInputScaling.value()
        self.get_zurichLockin["zurich_voltageInputAC"]= self.zurich_voltageInputAC.isChecked()
        self.get_zurichLockin["zurich_voltageInput50Ohm"] = self.zurich_voltageInput50Ohm.isChecked()
        self.get_zurichLockin["zurich_voltageInputDiff"] = self.zurich_voltageInputDiff.isChecked()
        self.get_zurichLockin["zurich_voltageInputFloat"] = self.zurich_voltageInputFloat.isChecked()
        self.get_zurichLockin["zurich_currentInputRange"] = self.zurich_currentInputRange.value()
        self.get_zurichLockin["zurich_currentInputAutoRange"] = self.zurich_currentInputAutoRange.isChecked()
        self.get_zurichLockin["zurich_currentInputScaling"] = self.zurich_currentInputScaling.value()
        self.get_zurichLockin["zurich_currentInputFloat"] = self.zurich_currentInputFloat.isChecked()
        self.get_zurichLockin["zurich_signalOutputON"] = self.zurich_signalOutputON.isChecked()
        self.get_zurichLockin["zurich_signalOutput50Ohm"] = self.zurich_signalOutput50Ohm.isChecked()
        self.get_zurichLockin["zurich_signalOutputRange"] = self.zurich_signalOutputRange.value()
        self.get_zurichLockin["zurich_signalOutputAdd"] = self.zurich_signalOutputAdd.isChecked()
        self.get_zurichLockin["zurich_signalOutputDiff"] = self.zurich_signalOutputDiff.isChecked()
        self.get_zurichLockin["zurich_oscillator1Freq"] = self.zurich_oscillator1Freq.value()
        self.get_zurichLockin["zurich_oscillator2Freq"] = self.zurich_oscillator2Freq.value()
        self.get_zurichLockin["zurich_oscillator3Freq"] = self.zurich_oscillator3Freq.value()
        self.get_zurichLockin["zurich_oscillator4Freq"] = self.zurich_oscillator4Freq.value()
        self.get_zurichLockin["zurich_demod1Osc"] = self.zurich_demod1Osc.currentIndex()
        self.get_zurichLockin["zurich_demod2Osc"] = self.zurich_demod2Osc.currentIndex()
        self.get_zurichLockin["zurich_demod3Osc"] = self.zurich_demod3Osc.currentIndex()
        self.get_zurichLockin["zurich_demod4Osc"] = self.zurich_demod4Osc.currentIndex()
        self.get_zurichLockin["zurich_demod1Harm"] = self.zurich_demod1Harm.currentIndex()
        self.get_zurichLockin["zurich_demod2Harm"] = self.zurich_demod2Harm.currentIndex()
        self.get_zurichLockin["zurich_demod3Harm"] = self.zurich_demod3Harm.currentIndex()
        self.get_zurichLockin["zurich_demod4Harm"] = self.zurich_demod4Harm.currentIndex()
        self.get_zurichLockin["zurich_demod1Signal"] = self.zurich_demod1Signal.currentIndex()
        self.get_zurichLockin["zurich_demod2Signal"] = self.zurich_demod2Signal.currentIndex()
        self.get_zurichLockin["zurich_demod3Signal"] = self.zurich_demod3Signal.currentIndex()
        self.get_zurichLockin["zurich_demod4Signal"] = self.zurich_demod4Signal.currentIndex()
        self.get_zurichLockin["zurich_demod1Enable"] = self.zurich_demod1Enable.isChecked()
        self.get_zurichLockin["zurich_demod2Enable"] = self.zurich_demod2Enable.isChecked()
        self.get_zurichLockin["zurich_demod3Enable"] = self.zurich_demod3Enable.isChecked()
        self.get_zurichLockin["zurich_demod4Enable"] = self.zurich_demod4Enable.isChecked()
        self.get_zurichLockin["zurich_demod1FilterOrder"] = self.zurich_demod1FilterOrder.currentIndex()
        self.get_zurichLockin["zurich_demod2FilterOrder"] = self.zurich_demod2FilterOrder.currentIndex()
        self.get_zurichLockin["zurich_demod3FilterOrder"] = self.zurich_demod3FilterOrder.currentIndex()
        self.get_zurichLockin["zurich_demod4FilterOrder"] = self.zurich_demod4FilterOrder.currentIndex()

        self.get_field["calconst"] = self.field_cal_const.value()
        self.get_field["delayafterfield"] = self.field_delay.value()
        self.get_field["fieldtozero"] = self.field_fieldtozero.isChecked()
        self.get_field["fieldslowstep"] = self.field_step.value()


    def FillFromFileSettings(self):

        self.set_general = Settings().settings["general"]
        self.set_source = Settings().settings["sourcemeter"]
        self.set_multimeter = Settings().settings["multimeter"]
        self.set_gaussmeter = Settings().settings["gaussmeter"]
        self.set_zurichLockin = Settings().settings["zurichLockin"]
        self.set_field = Settings().settings["field"]

        self.user_selector.setCurrentIndex(self.set_general["name"])
        self.path_text_line.setText(self.set_general["pathtosave"])

        self.source_selector.setCurrentIndex(self.set_source["selector"])
        self.source_port.setCurrentIndex(self.set_source["port"])
        self.source_NPLC.setValue(self.set_source["NPLC"])
        self.source_source.setCurrentIndex(self.set_source["source"])
        self.source_mode.setChecked(self.set_source["mode"])
        self.source_limit.setValue(self.set_source["limit"])
        self.source_average.setValue(self.set_source["average"])
        self.source_delay.setValue(self.set_source["delay"])

        self.multimeter_selector.setCurrentIndex(self.set_multimeter["selector"])
        self.multimeter_port.setCurrentIndex(self.set_multimeter["port"])
        self.multimeter_function.setCurrentIndex(self.set_multimeter["function"])
        self.multimeter_autorange.setChecked(self.set_multimeter["autorange"])
        self.multimeter_resolution.setValue(self.set_multimeter["resolution"])
        self.multimeter_range.setValue(self.set_multimeter["range"])
        self.multimeter_average.setValue(self.set_multimeter["average"])

        self.gaussmeter_selector.setCurrentIndex(self.set_gaussmeter["selector"])
        self.gaussmeter_port.setCurrentIndex(self.set_gaussmeter["port"])
        self.gaussmeter_autorange.setChecked(self.set_gaussmeter["autorange"])
        self.gaussmeter_dcresol.setValue(self.set_gaussmeter["DCresol"])
        self.gaussmeter_range.setValue(self.set_gaussmeter["range"])
        self.gaussmeter_average.setValue(self.set_gaussmeter["average"])

        self.lockin_selector.setCurrentIndex(self.set_zurichLockin["selector"])
        self.zurich_voltageInputRange.setValue(self.set_zurichLockin["zurich_voltageInputRange"])
        self.zurich_voltageInputAutoRange.setChecked(self.set_zurichLockin["zurich_voltageInputAutoRange"])
        self.zurich_voltageInputScaling.setValue(self.set_zurichLockin["zurich_voltageInputScaling"])
        self.zurich_voltageInputAC.setChecked(self.set_zurichLockin["zurich_voltageInputAC"])
        self.zurich_voltageInput50Ohm.setChecked(self.set_zurichLockin["zurich_voltageInput50Ohm"])
        self.zurich_voltageInputDiff.setChecked(self.set_zurichLockin["zurich_voltageInputDiff"])
        self.zurich_voltageInputFloat.setChecked(self.set_zurichLockin["zurich_voltageInputFloat"])
        self.zurich_currentInputRange.setValue(self.set_zurichLockin["zurich_currentInputRange"])
        self.zurich_currentInputAutoRange.setChecked(self.set_zurichLockin["zurich_currentInputAutoRange"])
        self.zurich_currentInputScaling.setValue(self.set_zurichLockin["zurich_currentInputScaling"])
        self.zurich_currentInputFloat.setChecked(self.set_zurichLockin["zurich_currentInputFloat"])
        self.zurich_signalOutputON.setChecked(self.set_zurichLockin["zurich_signalOutputON"])
        self.zurich_signalOutput50Ohm.setChecked(self.set_zurichLockin["zurich_signalOutput50Ohm"])
        self.zurich_signalOutputRange.setValue(self.set_zurichLockin["zurich_signalOutputRange"])
        self.zurich_signalOutputAdd.setChecked(self.set_zurichLockin["zurich_signalOutputAdd"])
        self.zurich_signalOutputDiff.setChecked(self.set_zurichLockin["zurich_signalOutputDiff"])
        self.zurich_oscillator1Freq.setValue(self.set_zurichLockin["zurich_oscillator1Freq"])
        self.zurich_oscillator2Freq.setValue(self.set_zurichLockin["zurich_oscillator2Freq"])
        self.zurich_oscillator3Freq.setValue(self.set_zurichLockin["zurich_oscillator3Freq"])
        self.zurich_oscillator4Freq.setValue(self.set_zurichLockin["zurich_oscillator4Freq"])
        self.zurich_demod1Osc.setCurrentIndex(self.set_zurichLockin["zurich_demod1Osc"])
        self.zurich_demod2Osc.setCurrentIndex(self.set_zurichLockin["zurich_demod2Osc"])
        self.zurich_demod3Osc.setCurrentIndex(self.set_zurichLockin["zurich_demod3Osc"])
        self.zurich_demod4Osc.setCurrentIndex(self.set_zurichLockin["zurich_demod4Osc"])
        self.zurich_demod1Harm.setCurrentIndex(self.set_zurichLockin["zurich_demod1Harm"])
        self.zurich_demod2Harm.setCurrentIndex(self.set_zurichLockin["zurich_demod2Harm"])
        self.zurich_demod3Harm.setCurrentIndex(self.set_zurichLockin["zurich_demod3Harm"])
        self.zurich_demod4Harm.setCurrentIndex(self.set_zurichLockin["zurich_demod4Harm"])
        self.zurich_demod1Signal.setCurrentIndex(self.set_zurichLockin["zurich_demod1Signal"])
        self.zurich_demod2Signal.setCurrentIndex(self.set_zurichLockin["zurich_demod2Signal"])
        self.zurich_demod3Signal.setCurrentIndex(self.set_zurichLockin["zurich_demod3Signal"])
        self.zurich_demod4Signal.setCurrentIndex(self.set_zurichLockin["zurich_demod4Signal"])
        self.zurich_demod1Enable.setChecked(self.set_zurichLockin["zurich_demod1Enable"])
        self.zurich_demod2Enable.setChecked(self.set_zurichLockin["zurich_demod2Enable"])
        self.zurich_demod3Enable.setChecked(self.set_zurichLockin["zurich_demod3Enable"])
        self.zurich_demod4Enable.setChecked(self.set_zurichLockin["zurich_demod4Enable"])
        self.zurich_demod1FilterOrder.setCurrentIndex(self.set_zurichLockin["zurich_demod1FilterOrder"])
        self.zurich_demod2FilterOrder.setCurrentIndex(self.set_zurichLockin["zurich_demod2FilterOrder"])
        self.zurich_demod3FilterOrder.setCurrentIndex(self.set_zurichLockin["zurich_demod3FilterOrder"])
        self.zurich_demod4FilterOrder.setCurrentIndex(self.set_zurichLockin["zurich_demod4FilterOrder"])

        self.field_cal_const.setValue(self.set_field["calconst"])
        self.field_delay.setValue(self.set_field["delayafterfield"])
        self.field_fieldtozero.setChecked(self.set_field["fieldtozero"])
        self.field_step.setValue(self.set_field["fieldslowstep"])


    def SaveSettings(self):
         self.read = self.ReadSettings()
         #self.set_gaussmeter = Settings().settings["gaussmeter"]
         #self.set_gaussmeter["average"] = self.gaussmeter_average.getValue()
         self.save = Settings().savetofile(self.path_text_line.text())

################################## Settings Behaviours ###########################################

    def pathbutton(self):
        startingDir = '/'
        dialog = QFileDialog()
        dialog.setFileMode(QFileDialog.FileMode())
        self.directory=dialog.getExistingDirectory(None, 'Open working directory', startingDir)
        self.path_text_line.setText(self.directory)
        return self.directory

    def initstack(self):
        self.source_stack.setCurrentIndex(1)
        self.multimeter_stack.setCurrentIndex(1)
        self.gaussmeter_stack.setCurrentIndex(1)
        self.stepper_stack.setCurrentIndex(1)
        self.lockin_stack.setCurrentIndex(2)
        self.lockin_slider1.setEnabled(0)
        self.field_stack.setCurrentIndex(1)

    def initcombo(self):
        self.source_selector.setCurrentIndex(0)
        self.multimeter_selector.setCurrentIndex(0)
        self.gaussmeter_selector.setCurrentIndex(0)
        self.generator_selector.setCurrentIndex(0)
        self.stepper_selector.setCurrentIndex(0)
        self.lockin_selector.setCurrentIndex(0)
        self.stage_selector.setCurrentIndex(0)
        self.temp_selector.setCurrentIndex(0)

    def sourceselector(self):
        if self.source_selector.currentIndex() == 0:
            self.source_stack.setCurrentIndex(1)
        elif self.source_selector.currentIndex() == 1:
            self.source_stack.setCurrentIndex(0)
        elif self.source_selector.currentIndex() == 2:
            self.source_stack.setCurrentIndex(0)

    def multimeterselector(self):
        if self.multimeter_selector.currentIndex() == 0:
            self.multimeter_stack.setCurrentIndex(1)
        elif self.multimeter_selector.currentIndex() == 1:
            self.multimeter_stack.setCurrentIndex(0)

    def gaussmeterselector(self):
        if self.gaussmeter_selector.currentIndex() == 0:
            self.gaussmeter_stack.setCurrentIndex(1)
        elif self.gaussmeter_selector.currentIndex() == 1:
            self.gaussmeter_stack.setCurrentIndex(0)
        elif self.gaussmeter_selector.currentIndex() == 2:
            self.gaussmeter_stack.setCurrentIndex(0)
        elif self.gaussmeter_selector.currentIndex() == 3:
            self.gaussmeter_stack.setCurrentIndex(0)
        elif self.gaussmeter_selector.currentIndex() == 4:
            self.gaussmeter_stack.setCurrentIndex(0)

    def stepperselector(self):
        if self.stepper_selector.currentIndex() == 0:
            self.stepper_stack.setCurrentIndex(1)
            self.par_steppermode.setEditable(0)
        elif self.stepper_selector.currentIndex() == 1:
            self.stepper_stack.setCurrentIndex(0)

    def fieldselector(self):
        if self.field_selector.currentIndex() == 0:
            self.field_stack.setCurrentIndex(1)
        elif self.field_selector.currentIndex() == 1:
            self.field_stack.setCurrentIndex(0)
        elif self.field_selector.currentIndex() == 1:
            self.field_stack.setCurrentIndex(0)

    def lockinselector(self):
        if self.lockin_selector.currentIndex() == 0:
            self.lockin_stack.setCurrentIndex(2)
            self.lockin_slider1.setEnabled(0)
        elif self.lockin_selector.currentIndex() == 1:
            self.lockin_stack.setCurrentIndex(2)
            self.lockin_slider1.setEnabled(0)
        elif self.lockin_selector.currentIndex() == 2:
            self.lockin_stack.setCurrentIndex(0)
            self.lockin_slider1.setEnabled(1)
            self.lockin_slider1.setValue(99)

################PARAMS BEHAVIOURS############################

    def source_unit_behaviour(self):
        self.par_source.setEnabled(1)
        if self.source_source.currentIndex() == 0:
            self.par_sourceunit.setText("A")
        else:
            self.par_sourceunit.setText("V")
        self.par_biasvalue.setDisabled(1)
        self.par_biasunit.setDisabled(1)

    def steppertypebehaviour(self):
        if self.par_steppermode.currentIndex() == 0:
            self.par_stimulusunit.setText("Oe")
            self.par_azim.setEnabled(1)
            self.par_polar.setEnabled(1)
        elif self.par_steppermode.currentIndex() == 1:
            self.par_stimulusunit.setText("deg")
            self.par_azim.setDisabled(1)
            self.par_polar.setEnabled(1)
        elif self.par_steppermode.currentIndex() == 2:
            self.par_stimulusunit.setText("deg")
            self.par_polar.setDisabled(1)
            self.par_azim.setEnabled(1)

    def tmrselect(self):
        self.par_name.setEnabled(1)
        self.par_stimulus.setEnabled(1)
        if self.par_steppermode.currentIndex() == 1:
            self.par_stimulusunit.setText("deg")
            self.par_azim.setDisabled(1)
            self.par_polar.setDisabled(1)
        else:
            self.par_stimulusunit.setText("Oe")
            if self.stepper_selector.currentIndex() == 0:
                self.par_azim.setDisabled(1)
                self.par_polar.setDisabled(1)
            else:
                self.par_azim.setEnabled(1)
                self.par_polar.setEnabled(1)

        self.par_frequency.setDisabled(1)
        self.par_temp.setDisabled(1)
        self.par_pulsewidth.setDisabled(1)
        self.par_pulseperiod.setDisabled(1)
        if self.par_steppermode.currentIndex() == 0:
            self.par_fieldbias.setDisabled(1)
        else:
            self.par_fieldbias.setEnabled(1)
        self.par_fieldangle.setDisabled(1)


################Control experiment############################







if __name__=='__main__':
    app = QtWidgets.QApplication(sys.argv)
    example = Example(app)
    sys.exit(app.exec_())