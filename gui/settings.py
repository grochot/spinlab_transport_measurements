import json

class Settings:
    settings = {
        "general":
        {
            "name":0,
            "pathtosave": "/"

        },
        "sourcemeter":
        {
            "selector": 0,
            "port": 0,
            "source": 1,
            "mode": False,
            "limit": 0.01,
            "NPLC": 0.01,
            "average": 1,
            "delay": 100
        },
        "multimeter":
        {
            "selector": 0,
            "port": 0,
            "function": 0,
            "autorange":False,
            "resolution": 5.5,
            "range": 5,
            "average": 1,
        },
        "gaussmeter":
        {
            "selector": 0,
            "port": 0,
            "autorange": False,
            "DCresol": 0.001,
            "range": 4,
            "average": 1
        },
        "zurichLockin":
        {"selector": 0,
         "zurich_voltageInputRange": 0.0,
         "zurich_voltageInputAutoRange": True,
         "zurich_voltageInputScaling": 0.0,
         "zurich_voltageInputAC": False,
         "zurich_voltageInput50Ohm": False,
         "zurich_voltageInputDiff": False,
         "zurich_voltageInputFloat": False,
         "zurich_currentInputRange": 0.0,
         "zurich_currentInputAutoRange": False,
         "zurich_currentInputScaling": False,
         "zurich_currentInputFloat": False,
         "zurich_signalOutputON": False,
         "zurich_signalOutput50Ohm": False,
         "zurich_signalOutputRange": 0.0,
         "zurich_signalOutputAdd": False,
         "zurich_signalOutputDiff": False,
         "zurich_oscillator1Freq": 0.0,
         "zurich_oscillator2Freq": 0.0,
         "zurich_oscillator3Freq": 0.0,
         "zurich_oscillator4Freq": 0.0,
         "zurich_demod1Osc": 0,
         "zurich_demod2Osc": 1,
         "zurich_demod3Osc": 1,
         "zurich_demod4Osc": 1,
         "zurich_demod1Harm": 1,
         "zurich_demod2Harm": 1,
         "zurich_demod3Harm": 1,
         "zurich_demod4Harm": 1,
         "zurich_demod1Signal": 1,
         "zurich_demod2Signal": 1,
         "zurich_demod3Signal": 1,
         "zurich_demod4Signal": 1,
         "zurich_demod1Enable": False,
         "zurich_demod2Enable": False,
         "zurich_demod3Enable": False,
         "zurich_demod4Enable": False,
         "zurich_demod1FilterOrder": 1,
         "zurich_demod2FilterOrder": 1,
         "zurich_demod3FilterOrder": 1,
         "zurich_demod4FilterOrder": 1
         },
        "field":
        {
            "calconst" : 0,
            "selector": 0,
            "fieldslowstep":0,
            "delayafterfield":0,
            "fieldtozero":True
        }
    }
    def __init__(self):
        with open("settings.json", "w") as sett:
            json.dump(self.settings,sett)

    def savetofile(self,path):
        with open(str(path)+"/"+"settings.json", "w") as sett:
            json.dump(self.settings,sett)

    def addtofile(self, key, value):
        self.settings[key] = str(value)

    def readfromfile(self, path):
        with open(str(path)+"/"+"settings.json", "r") as sett:
            settings = json.load(sett)
        return settings