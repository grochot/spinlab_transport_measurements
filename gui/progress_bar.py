from gui.gui_test import Window
from procedure.one_step_time import Onesteptime

class ProgressBar:
    initvalue = 0

    def update(self):
        self.steptime = Onesteptime.result_step
        if self.initvalue < 100:
            self.initvalue += self.steptime
            Window.ProgressBar.setValue(self.initvalue)
