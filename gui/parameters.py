import json

class Parameters:
    parameters = {"name": "",
                       "procedure": "",
                       "steppermode": "",
                       "stimulus": "",
                       "source": "",
                       "biasvalue": "",
                       "flip": "",
                       "azim": "",
                       "polar": "",
                       "freq": "",
                       "temp": "",
                       "pulsewidth": "",
                       "pulseperiod": "",
                       "fieldbias": "",
                       "fieldangle": "",
                       }
    def __init__(self):
        with open("parameter.json", "w") as param:
            json.dump(self.parameters,param)

    def savetofile(self):
        with open("parameter.json", "w") as param:
            json.dump(self.parameters,param)

    def addtofile(self, key, value):
        self.parameters[key] = str(value)

    def readfromfile(self):
        with open("parameter.json", "r") as param:
            parameters = json.load(param)
        return parameters

    def readvalue(self, key):
        with open("parameter.json", "r") as param:
            value = json.load(param)
        return(value[key])




