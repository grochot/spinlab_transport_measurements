from hardware.instrument import Instrument
import numpy as np
import time


class Agilent3648(Instrument):
    def Volt(self, volt):
        self.write("VOLT" + " " + str(volt))
    def Amp(self, amp):
        self.write("CURR" + " " + str(amp))
    def OutOn(self):
        self.write("OUTP ON")
    def OutOff(self):
        self.write("OUTP OFF")
    def Idn(self):
        self.ask("*IDN?")
    def InsSel(self, ins):
        self.write("INST:NSEL "+str(ins))



    def __init__(self, adapter, **kwargs):
        super(Agilent3648, self).__init__(
            adapter, "Agilent3648", **kwargs
        )


agilent = Agilent3648('GPIB0::04::INSTR')
agilent.InsSel(1)

vector = [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1]

agilent.OutOn()
agilent.InsSel(2)
agilent.OutOn()
t = 1
while t < 100:
    for i in range(0,10):
        time.sleep(0.05)
        agilent.InsSel(1)
        agilent.Volt(vector[i])
        agilent.InsSel(2)
        agilent.Volt(vector[i])
    t += 1

