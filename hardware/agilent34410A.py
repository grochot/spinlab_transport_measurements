from hardware.instrument import Instrument
import time

class Agilent34410A(Instrument):
    """
    Represent the HP/Agilent/Keysight 34410A and related multimeters.

    Implemented measurements: voltage_dc, voltage_ac, current_dc, current_ac, resistance, resistance_4w
    """
    #only the most simple functions are implemented
    voltage_dc = Instrument.measurement("MEAS:VOLT:DC? DEF,DEF", "DC voltage, in Volts")
    
    voltage_ac = Instrument.measurement("MEAS:VOLT:AC? DEF,DEF", "AC voltage, in Volts")
    
    current_dc = Instrument.measurement("MEAS:CURR:DC? DEF,DEF", "DC current, in Amps")
    
    current_ac = Instrument.measurement("MEAS:CURR:AC? DEF,DEF", "AC current, in Amps")
    
    resistance = Instrument.measurement("MEAS:RES? DEF,DEF", "Resistance, in Ohms")
    
    resistance_4w = Instrument.measurement("MEAS:FRES? DEF,DEF", "Four-wires (remote sensing) resistance, in Ohms")

    def initiate(self):
        self.write("INIT")

    def triggersource(self):
        self.write("TRIG:SOUR IMM")

    def trigdelay(self):
        self.write("TRIG:DEL:AUTO ON")

    def samplenum(self):
        self.write("SAMP:COUN 3")

    def triggnum(self):
        self.write("TRIG:COUN 1")

    def read(self):
        self.wynik = self.ask("READ?")
        return self.wynik

    def fetc(self):
        self.wynik2 = self.ask("FETC?")
        return self.wynik2

    def __init__(self, adapter, delay=0.02, **kwargs):
        super(Agilent34410A, self).__init__(
            adapter, "HP/Agilent/Keysight 34410A Multimiter", **kwargs
        )



