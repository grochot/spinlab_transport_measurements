from hardware.instrument import Instrument
import time
from hardware import lakeshore475


#FIELD CONTROLER

class Ametek5209(Instrument):
    def set_field(self, voltage):
        if voltage > 10:
            voltage = 0
            self.write("DAC 1" + " " + str((voltage*1000)))
        else:
            self.write("DAC 1"+" "+str((voltage*1000)))

    def ask_field(self):
        act_voltage = self.write("DAC")
        return act_voltage

    def fieldtozero(self):
        for k in range(9, -1, -1):
            self.set_field(((((lakeshore475.Field() / 590)) / 10)) * (k))
            time.sleep(1)
    
    def __init__(self, adapter, **kwargs):
        super(Ametek5209, self).__init__(
            adapter, "Ametek5209", **kwargs
        )

