from hardware.instrument import Instrument

class Lakeshore475(Instrument):
    def Range(self, range_int=5): 
        self.write("RANGE"+ " "+str(range_int))
    
    def Unit(self, unit_int):
        self.write("UNITS" + " " + str(unit_int))
    def Zclear(self):
        self.write("ZCLEAR") 
    def Zprobe(self):
        self.write("ZPROBE")
    def Field(self):                                               #READ FIELD
        self.res = self.ask("RDGFIELD?")
        return( float(self.res.replace("\r\n", "")))


    def __init__(self, adapter, **kwargs):
        super(Lakeshore475, self).__init__(
            adapter, "Lakeshore475", **kwargs
        )


    

    





# lakeshore = rm.open_resource('GPIB0::12::INSTR')
# print(lakeshore.query('*IDN?'))
# lakeshore.write("RANGE 5") 
# lakeshore.write("UNIT 3") 
# #lakeshore.write("ZCLEAR")
# #lakeshore.write("ZPROBE")
# print(lakeshore.query("RDGFIELD?")) 
# lakeshore.write("DLOGSET 4") 
# lakeshore.write("TRIG 1")
# lakeshore.write("DLOG 1") 
# time.sleep(5)
# lakeshore.write("DLOG 0") 
# print(lakeshore.query("DLOGSET?"))
# print(lakeshore.query("DLOGNUM?")) 
# print(lakeshore.query("DLOGRDG? 0000"))
