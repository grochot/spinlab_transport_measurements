'''GPIB driver for controlling Keithley 26xx sourcemeter unit.
The class is used to create a connection to Keithley 24xx sourcemeter units via GPIB

The GPIB driver uses TSP scripting language for communicating with the instrument.
See more information in the reference manual at
http://www.tek.com/manual/series-2600b-system-sourcemeter-instruments-reference-manual

Example:
    import keithley_visa_drivers as ke

    smu = ke.ke26XX.sourcemeter()
    smu.setVolts(1, 'smua')
    smu.ON()
    voltage, current = smu.getVI('smua')
    smu.OFF()
    print(voltage, current)

'''

import visa
import sys


class Sourcemeter(object):
    '''class for Keithley 26xx type SourceMeter units.
    '''

    def __init__(self, portNum=26):
        '''
        Initialize GPIB connection. Default GPIB address is set to 26.
        '''

        self._GPIBaddr_ = str(portNum)

        self.rm = visa.ResourceManager()
        # list available GPIB ports
        # ports = rm.list_resources()
        # check if portNum matches any available port
        # print "Checking the available ports..."
        self._GPIBport_ = 'GPIB0::{}::INSTR'.format(self._GPIBaddr_)

        # for p in ports:
        #
        #    if self._GPIBaddr_ in p:
        #        self._GPIBport_ = p
        #        print "GPIB found at " + self._GPIBport_
        #
        # if not self._GPIBport_:
        #    print "GPIB NOT FOUND AT THE USER-DEFINED ADDRESS! CHECK THE ADDRESS!"
        #    exit(0)
        try:
            self.openport()
        except ValueError:
            print('GPIB not found at {}. Trying to find it otherwise.'.format(self._GPIBport_))
            self._GPIBport_ = None

            # list available GPIB ports
            ports = self.rm.list_resources()
            # check if portNum matches any available port
            print("Checking the available ports...")

            for p in ports:
                if self._GPIBaddr_ in p:
                    self._GPIBport_ = p
                    print("GPIB found at " + self._GPIBport_)
                    self.openport()

            if not self._GPIBport_:
                sys.exit("GPIB NOT FOUND AT THE USER-DEFINED ADDRESS! CHECK THE ADDRESS!")

        self._device_ = self.raw.query('*IDN?')
        if 'Keithley' in self._device_:
            print(self._device_.rstrip('\n'))
        else:
            print("Keithley was not found at '{}'! Try another GPIB address.".format(self._GPIBport_))
            exit(0)

    def openport(self):
        '''
        Open GPIB connection
        '''
        self.raw = self.rm.open_resource(self._GPIBport_)

    def closeport(self):
        '''
        Close GPIB connection
        '''
        self.raw.close()

    def getVolts(self, channel="smua"):
        """Measure one sample of voltage and returns the float.

        Parameter:
            channel (str): Channel (smua/smub) for which the measurement is performed.

        Returns:
            voltage (float): Voltage value obtained from the channel.
        """
        line = self.raw.query("print({}.measure.v())".format(channel))
        voltage = float(line.rstrip('\n'))
        return voltage

    def getCurrent(self, channel="smua"):
        """Measure one sample of current and returns the float.

        Parameter:
            channel (str): Channel (smua/smub) for which the measurement is performed.

        Returns:
            current (float): Current value obtained from the channel.
        """
        line = self.raw.query("print({}.measure.i())".format(channel))
        current = float(line.rstrip('\n'))
        return current

    def getVI(self, channel="smua"):
        """Measure one sample pair of current and voltage values, and return the tuple of floats.

        Parameter:
            channel (str): Channel (smua/smub) for which the measurement is performed.

        Returns:
            current, voltage ((float, float)): Current and voltage values obtained from the channel.
        """
        line = self.raw.query("print({}.measure.iv())".format(channel))
        current, voltage = [float(x) for x in line.split()]
        return (current, voltage)

    def getResistance(self, channel="smua"):
        """Measure one sample of resistance and returns the float.

        Parameter:
            channel (str): Channel (smua/smub) for which the measurement is performed.

        Returns:
            resistance (float): Resistance value obtained from the channel.
        """
        line = self.raw.query("print({}.measure.r())".format(channel))
        resistance = float(line.rstrip('\n'))
        return resistance

    # set output values
    def setVolts(self, volts, channel="smua"):
        """Set source voltage.

        Parameters:
            volts (float): Voltage value set by the user.
            channel (str): Channel (smua/smub) for which the measurement is performed.
        """
        self.raw.write('{}.source.levelv = {}'.format(channel, volts))

    def setCurrent(self, current, channel="smua"):
        """Set source current.

        Parameters:
            current (float): current value set by the user
            channel (str): Channel (smua/smub) for which the measurement is performed.
        """
        self.raw.write('{}.source.leveli = {}'.format(channel, current))

    # set output on/off
    def ON(self, channel="both"):
        '''Set channel output ON. By default it enables both channels simultaneously.

        Parameters:
            channel (str): Channel (smua/smub/both) for which the measurement is performed.

        '''
        if "both" in channel:
            self.raw.write("smua.source.output = 1")
            self.raw.write("smub.source.output = 1")
        else:
            self.raw.write("{}.source.output = 1".format(channel))

    def OFF(self, channel="both"):
        '''Set channel output OFF. By default it disables both channels simultaneously.

        Parameters:
            channel (str): Channel (smua/smub/both) for which the measurement is performed.

        '''
        if "both" in channel:
            self.raw.write("smua.source.output = 0")
            self.raw.write("smub.source.output = 0")
        else:
            self.raw.write("{}.source.output = 0".format(channel))

    def setSourceFunc(self, mode='VOLTS', channel='both'):
        '''Set source mode VOLTS/AMPS for channels both/smua/smub.

        Parameters:
            mode (str): Source mode
            channel (str): Channel to be set
        '''

        if 'both' in channel:
            self.raw.write('smua.source.func = smua.OUTPUT_DC{}'.format(mode))
            self.raw.write('smub.source.func = smub.OUTPUT_DC{}'.format(mode))
        else:
            self.raw.write('{}.source.func = {}.OUTPUT_DC{}'.format(channel, channel, mode))

    def vCompliance(self, volts, channel="both"):
        '''Set Voltage Compliance level for channels both/smua/smub.

        Parameters:
            volts (float): Voltage compliance level set by the user
            channel (str): Channel to be set
        '''

        if "both" in channel:
            self.raw.write('smua.source.limitv = {}'.format(volts))
            self.raw.write('smub.source.limitv = {}'.format(volts))
        else:
            self.raw.write('{}.source.limitv = {}'.format(channel, volts))

    def cCompliance(self, current, channel="both"):
        '''Set Current Compliance level for channels both/smua/smub.

        Parameters:
            current (float): Current compliance level set by the user
            channel (str): Channel to be set
        '''
        if "both" in channel:
            self.raw.write('smua.source.limiti = {}'.format(current))
            self.raw.write('smub.source.limiti = {}'.format(current))
        else:
            self.raw.write('{}.source.limiti = {}'.format(channel, current))

    def beeper_enable(self, enable=0):
        '''Enable(1)/Disable(0) the beeper in the instrument

        Parameter:
            enable (int): State of the beeper 0=disabled and 1=enabled
        '''
        enable = int(enable)
        self.raw.write('beeper.enable = {}'.format(enable))

    def displayFunc(self, mode="AMPS", channel="both"):
        """Set front panel measurement display to AMPS/VOLTS for channels both/smua/smub.

        Parameters:
            mode (str): Mode of display in the front panel
            channel (str): Channel to be set
        """
        if "both" in channel:
            self.raw.write('display.smua.measure.func = display.MEASURE_DC{}'.format(mode))
            self.raw.write('display.smub.measure.func = display.MEASURE_DC{}'.format(mode))
        else:
            self.raw.write('display.{}.measure.func = display.MEASURE_DC{}'.format(channel, mode))

    def sourceDelay(self, delay=0, channel="both"):
        '''Set additional source settling time to the channel(s)

        Parameters:
            delay (float): settling time (-1 = AUTO, 0 = OFF)
            channel (str): Channel (smua/smub/both) to be set
        '''
        if "both" in channel:
            self.raw.write('smua.source.delay = {}'.format(delay))
            self.raw.write('smub.source.delay = {}'.format(delay))
        else:
            self.raw.write('{}.source.delay = {}'.format(channel, delay))

    def measDelay(self, delay=-1, delayfactor=1.0, channel="both"):
        '''Set additional measurement settling time to the channel(s)

        Parameters:
            delay (float): settling time (-1 = AUTO, 0 = OFF)
            channel (str): Channel (smua/smub/both) to be set
        '''
        if "both" in channel:
            self.raw.write('smua.meas.delay = {}'.format(delay))
            self.raw.write('smub.meas.delay = {}'.format(delay))
        else:
            self.raw.write('{}.meas.delay = {}'.format(channel, delay))

        if delay == -1 and 'both' in channel:
            self.raw.write('smua.meas.delayfactor = {}'.format(delayfactor))
            self.raw.write('smub.meas.delayfactor = {}'.format(delayfactor))
        elif delay == -1:
            self.raw.write('{}.meas.delayfactor = {}'.format(channel, delayfactor))

    def setVoltSourceRange(self, mode=1, channel="both", _range=1):
        '''
        Set range mode for voltage source
            ON=1=autorange
            OFF=0=no autorange,requires value for _range.
        for channel both/smua/smub
        '''
        if "both" in channel:
            self.raw.write('smua.source.autorangev = {}'.format(mode))
            self.raw.write('smub.source.autorangev = {}'.format(mode))
        else:
            self.raw.write('{}.source.autorangev = {}'.format(channel, mode))

        if not mode and "both" in channel:
            self.raw.write('smua.source.rangev = {}'.format(_range))
            self.raw.write('smub.source.rangev = {}'.format(_range))
        elif not mode:
            self.raw.write('{}.source.rangev = {}'.format(channel, _range))

    def setCurrSourceRange(self, mode=1, channel="both", _range=1e-3):
        '''
        Set range mode for current source
            ON=1=autorange
            OFF=0=no autorange,requires value for _range.
        for channel both/smua/smub
        '''
        if "both" in channel:
            self.raw.write("smua.source.autorangei = {}".format(mode))
            self.raw.write("smub.source.autorangei = {}".format(mode))
        else:
            self.raw.write("{}.source.autorangei = {}".format(channel, mode))

        if not mode and "both" in channel:
            self.raw.write("smua.source.rangei = {}".format(_range))
            self.raw.write("smub.source.rangei = {}".format(_range))
        elif not mode:
            self.raw.write("{}.source.rangei = {}".format(channel, _range))

    def setMeasAutoZero(self, autozero="ONCE", channel="both"):
        '''
        Set measurement autozero setting to ONCE/OFF/AUTO for channels both/smua/smub.
        '''
        if "both" in channel:
            self.raw.write('smua.measure.autozero = {}'.format(autozero))
            self.raw.write('smub.measure.autozero = {}'.format(autozero))
        else:
            self.raw.write('{}.source.autozero = {}'.format(channel, autozero))

    def setSenseMode(self, mode="LOCAL", channel="both"):
        '''
        Set the sensing mode LOCAL=2-wire or REMOTE=4-wire for channel both/smua/smub
        '''
        if "both" in channel:
            self.raw.write('smua.sense = smua.SENSE_{}'.format(mode))
            self.raw.write('smub.sense = smub.SENSE_{}'.format(mode))
        else:
            self.raw.write('{}.sense = {}.SENSE_{}'.format(channel, channel, mode))

    def setCurrSenseRange(self, mode=1, channel='both', _range=1e-3):
        '''
        Set range for current measurements
            1=ON=autoranging,
            0=OFF=no autoranging and requires value for _range.
        for channel both/smua/smub
        '''
        if "both" in channel:
            self.raw.write('smua.measure.autorangei = {}'.format(mode))
            self.raw.write('smub.measure.autorangei = {}'.format(mode))
        else:
            self.raw.write('{}.measure.autorangei = {}'.format(channel, mode))

        if not mode and "both" in channel:
            self.raw.write('smua.measure.rangei = {}'.format(_range))
            self.raw.write('smub.measure.rangei = {}'.format(_range))
        elif not mode:
            self.raw.write('{}.measure.rangei = {}'.format(channel, _range))

    def setVoltSenseRange(self, mode=1, channel='both', _range=1):
        '''
        Set range for voltage sourcing
            ON=1=autoranging,
            OFF=0=no autoranging and requires value for _range.
        for channel both/smua/smub
        '''
        if "both" in channel:
            self.raw.write('smua.measure.autorangev = {}'.format(mode))
            self.raw.write('smub.measure.autorangev = {}'.format(mode))
        else:
            self.raw.write('{}.measure.autorangev = {}'.format(channel, mode))

        if not mode and "both" in channel:
            self.raw.write('smua.measure.rangev = {}'.format(_range))
            self.raw.write('smub.measure.rangev = {}'.format(_range))
        elif not mode:
            self.raw.write('{}.measure.rangev = {}'.format(channel, _range))

    def reset(self):
        self.raw.write('smua.reset()')
        self.raw.write('smub.reset()')

    def setAnalogFilter(self, filter=1, channel="both"):
        '''
        Settings for analog filter
            filter=1 (default)
            filter=0 (disables the filter and speeds up the measurement but decreases accuracy)
        '''
        if "both" in channel:
            self.raw.write('smua.measure.analogfilter = {}'.format(filter))
            self.raw.write('smub.measure.analogfilter = {}'.format(filter))
        else:
            self.raw.write('{}.measure.analogfilter = {}'.format(channel, filter))

    def setNPLC(self, nplc=1.0, channel='both'):
        '''
        Set NPLC for channel both/smua/smub
        '''
        if "both" in channel:
            self.raw.write('smua.measure.nplc = {}'.format(nplc))
            self.raw.write('smub.measure.nplc = {}'.format(nplc))
        else:
            self.raw.write('{}.measure.nplc = {}'.format(channel, nplc))

    def command(self, command):
        '''
        send the supplied string to the instrument
        '''
        self.raw.write(command)

    def query(self, string):
        '''
        send the supplied string to the instrument
        '''
        self.raw.query(string)